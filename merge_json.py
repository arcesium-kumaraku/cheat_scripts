import json
import argparse
import time
import os
import shutil


__author__ = 'guptaksh'


def merge_json_files(input, result={}):
    if not os.path.exists(input):
        return result
    for file in [i for i in os.listdir(input) if i.endswith('.json')]:
        with open(os.path.join(input, file), 'r') as f:
            data = json.load(f)
        name = os.path.splitext(file)[0]
        if name in result:
            result[name].extend(data)
        else:
            result[name] = data
    return result


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--start-day', dest='start_day', default=1, required=True)
    parser.add_argument('--end-day', dest='end_day', default=1, required=True)
    parser.add_argument('--input-path', dest='input_path', required=True)
    return parser.parse_args()


def main():
    args = get_args()
    start_day = day = int(args.start_day)
    end_day = int(args.end_day)

    if start_day > end_day:
        raise ValueError('start_day must be less than end_day')
    if start_day < 1 or end_day < 1:
        raise ValueError('start_day & end_day must be greater than 1')
    if start_day == end_day:
        raise ValueError('start_day must be greter than end_day')

    input_path = args.input_path

    if os.path.exists(input_path):
        result = {}
        while (day <= end_day):
            result = merge_json_files(os.path.join(input_path, f'random_{day}_btc_1'), result)
            day += 1
    else:
        raise FileNotFoundError(f'{input_path} not found')
    
    dest_path = os.path.join(input_path, f'combined_{start_day}_{end_day}')
    if os.path.exists(dest_path):
        shutil.rmtree(dest_path)
    os.makedirs(dest_path)
    for file, data in result.items():
        with open(os.path.join(dest_path, f'{file}.json'), 'w') as combined:
            json.dump(data, combined, indent=2)


if __name__ == '__main__':
    '''
    python3 merge_json.py --start-day 1 --end-day 2 --input-path /home/guptaksh/Documents/JSON_TEST_SHORT-24_final_v6/S_1
    '''
    
    t0 = time.time()
    main()
    print(f'{time.time()- t0} seconds')
