import pandas as pd
import os
import math


base_path = '/home/guptaksh/Documents/nov_amends/TEST_S_csf_amends'
perc_change = 10

for folder in [i for i in os.listdir(base_path) if os.path.isdir(os.path.join(base_path, i))]:
    file_path = os.path.join(base_path, folder, 'input', 'mat.csv')
    if os.path.exists(file_path):
        df = pd.read_csv(file_path, keep_default_na=False)
        os.remove(file_path)

        df_top = df.copy()
        df_top = df_top.head(math.ceil(len(df_top.index) * perc_change / 100))
        df_top['settle_date'] = ''

        df_bottom = df.copy()
        df_bottom = df_bottom.head(int(len(df_bottom.index) * (100 - perc_change) / 100))

        df_final = pd.concat([df_top, df_bottom], ignore_index=True)

        if len(df_final.index) > 0:
            df_final.to_csv(os.path.join(base_path, folder, 'input', 'mat.csv'), index=False)
