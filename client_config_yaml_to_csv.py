import pandas as pd
import yaml
from yaml.loader import SafeLoader
import os


base_path = '/home/guptaksh/code/change-management/client-config/prod'
input_yaml_file = 'client-config.yaml'

with open(os.path.join(base_path, input_yaml_file)) as f:
    data = yaml.load(f, Loader=SafeLoader)

data_clients = {**{'default-config': data['default-config']}, **data['clients']}

config = set()
for k in data_clients:
    config.update(list(data_clients[k].keys()))

df = pd.DataFrame(columns=['client_name', *config])

for k, v in data_clients.items():
    df = df.append({'client_name': k, **v}, ignore_index=True)

df.to_csv(os.path.join(base_path, 'client-config-normalize.csv'), index=False)
