import json
import os
import requests
from requests_kerberos import HTTPKerberosAuth, OPTIONAL
from datetime import datetime
import shutil


folder_path = '/home/guptaksh/Documents/Datasets/openspec_jsons/Day2_10'
dest_folder_path = os.path.join(folder_path + '_converted')

if os.path.exists(dest_folder_path):
    shutil.rmtree(dest_folder_path)
os.mkdir(dest_folder_path)

reference_data_map = {
    "book_name": {
      "BOOK_1": "TestBook_trinity_20_02"
    },
    "bundle_id": {
      "BUNDLE_1": "TestBundle1_trinity_20_02",
      "BUNDLE_2": "TestBundle2_trinity_20_02",
      "BUNDLE_3": "TestBundle3_trinity_20_02",
	  "BUNDLE_4": "TestBundle4_trinity_20_02",
	  "BUNDLE_5": "TestBundle5_trinity_20_02",
	  "BUNDLE_6": "TestBundle6_trinity_20_02",
	  "BUNDLE_7": "TestBundle7_trinity_20_02",
	  "BUNDLE_8": "TestBundle8_trinity_20_02",
	  "BUNDLE_9": "TestBundle9_trinity_20_02",
	  "BUNDLE_10": "TestBundle10_trinity_20_02"
    },
    "accountId": {
        "CA_1": "CA_1_trinity_20_02",
        "CA_2": "CA_2_trinity_20_02",
		"CA_3": "CA_3_trinity_20_02",
		"CA_4": "CA_4_trinity_20_02",
		"CA_5": "CA_5_trinity_20_02",
		"CA_1_TI": "CA_1_TI_trinity_20_02",
        "CA_2_TI": "CA_2_TI_trinity_20_02",
		"CA_3_TI": "CA_3_TI_trinity_20_02",
		"CA_4_TI": "CA_4_TI_trinity_20_02",
		"CA_5_TI": "CA_5_TI_trinity_20_02"

    },
    "spn": {
        "EQUITY_1" : "EQUITY_1_trinity_20_02",
        "EQUITY_2" : "EQUITY_1_trinity_20_02",
        "EQUITY_3" : "EQUITY_1_trinity_20_02",
        "EQUITY_4" : "EQUITY_1_trinity_20_02",
        "EQUITY_5" : "EQUITY_1_trinity_20_02",
        "FUTURE_1" : "FUTURE_1_trinity_20_02",
        "FUTURE_2" : "FUTURE_2_trinity_20_02",
        "FUTURE_3" : "FUTURE_3_trinity_20_02",
        "FUTURE_4" : "FUTURE_4_trinity_20_02",
        "FUTURE_5" : "FUTURE_5_trinity_20_02",
        "OPTION_1" : "OPTION_1_trinity_20_02",
        "OPTION_2" : "OPTION_2_trinity_20_02",
        "OPTION_3" : "OPTION_3_trinity_20_02",
        "OPTION_4" : "OPTION_4_trinity_20_02",
        "OPTION_5" : "OPTION_5_trinity_20_02",
        "EQSWAP_1" : "EQSWAP_1_trinity_20_02",
        "EQSWAP_2" : "EQSWAP_2_trinity_20_02",
        "EQSWAP_3" : "EQSWAP_3_trinity_20_02",
        "EQSWAP_4" : "EQSWAP_4_trinity_20_02",
        "EQSWAP_5" : "EQSWAP_5_trinity_20_02",
        "CCYFWD_1" : "CCYFWD_1_trinity_20_02",
        "BOND_1" : "BOND_1_trinity_20_02",
        "BOND_2" : "BOND_2_trinity_20_02",
        "BOND_3" : "BOND_3_trinity_20_02",
        "BOND_4" : "BOND_4_trinity_20_02",
        "BOND_5" : "BOND_5_trinity_20_02",
        "BOND_6" : "BOND_6_trinity_20_02",
        "BOND_7" : "BOND_7_trinity_20_02",
        "BOND_8" : "BOND_8_trinity_20_02",
        "BOND_9" : "BOND_9_trinity_20_02",
        "BOND_10" : "BOND_10_trinity_20_02",
        "BOND_11" : "BOND_11_trinity_20_02",
        "BOND_12" : "BOND_12_trinity_20_02",
        "BOND_13" : "BOND_13_trinity_20_02",
        "BOND_14" : "BOND_14_trinity_20_02",
        "BOND_15" : "BOND_15_trinity_20_02",
        "BOND_16" : "BOND_16_trinity_20_02",
        "BOND_17" : "BOND_17_trinity_20_02",
        "BOND_18" : "BOND_18_trinity_20_02",
        "BOND_19" : "BOND_19_trinity_20_02",
        "BOND_20" : "BOND_20_trinity_20_02",
        "BOND_21" : "BOND_21_trinity_20_02",
        "BOND_22" : "BOND_22_trinity_20_02",
        "BOND_23" : "BOND_23_trinity_20_02",
        "BOND_24" : "BOND_24_trinity_20_02",
        "BOND_25" : "BOND_25_trinity_20_02",
        "BOND_26" : "BOND_26_trinity_20_02",
        "BOND_27" : "BOND_27_trinity_20_02",
        "BOND_28" : "BOND_28_trinity_20_02",
        "BOND_29" : "BOND_29_trinity_20_02",
        "BOND_30" : "BOND_30_trinity_20_02"
    }   
  }

url_dict = {
    "currency": ["https://moss.{}.c.ia55.net/service/refService/getActiveCurrencies?format=JSON", "isoCode", "spn"],
    "book": ["http://moss.{}.c.ia55.net/service/refService/getAllBooks?format=json", "name", "id"],
    "bundle": ["https://moss.{}.c.ia55.net/service/bundleService/getBundles?includeRetired=false&format=JSON", "name", "id"],
    "ca": ["https://moss.{}.c.ia55.net/service/custodianAccountRefService/getAllCustodianAccounts?format=json", "displayName", "custodianAccountId"],
    "org": ["https://moss.{}.c.ia55.net/service/orgService/getOrganizationsByOrgType?orgType=-1&includeDead=false&format=JSON", "name", "id"],
}

SPN_ID_URL_TEMPLATE = "http://moss.<pod>.c.ia55.net/service/securityService/searchSpns?filter=bbg_unique_id.like(<unique_key>)&format=JSON"
SPN_DETAIL_URL_TEMPLATE = "http://moss.<pod>.c.ia55.net/service/securityService/getBasicSecurities?spns=<spn_list>&format=JSON"


def names_to_ids(pod, unique_key):
    result_dict = dict()

    for key, value in url_dict.items():
        res = requests.get(value[0].format(pod), auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL))
        name_id_dict = dict()

        for data in res.json():
            if value[1] in data and value[2] in data:
                name_id_dict[data[value[1]]] = data[value[2]]

        result_dict[key] = name_id_dict
    
    spn_url =  SPN_ID_URL_TEMPLATE.replace('<pod>',pod).replace('<unique_key>',unique_key)
    req = requests.get(spn_url, auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL))
    filtered_spn_list = json.loads(req.content)

    spn_list_txt = ''
    for spn in filtered_spn_list:
        spn_list_txt = spn_list_txt + str(spn) + ','

    spn_list_url = SPN_DETAIL_URL_TEMPLATE.replace('<pod>',pod).replace('<spn_list>',spn_list_txt)
    spn_list_req = requests.get(spn_list_url, auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL))
    spn_map = dict()
    for per_spn_detail in spn_list_req.json():
        if 'spn' in per_spn_detail and 'desname' in per_spn_detail:
            spn_map[per_spn_detail['desname']] = per_spn_detail['spn']

    result_dict['spn'] = spn_map

    return result_dict


t0 = datetime.now()

blind_copy_files = ['fwd_rates.json', 'fx_rates.json', 'CURRENCY.json']
for blind_copy in blind_copy_files:
    if os.path.exists(os.path.join(folder_path, blind_copy)):
        shutil.copy(os.path.join(folder_path, blind_copy), os.path.join(dest_folder_path, blind_copy))

replace_dict = names_to_ids('europa', '%25_trinity_20_02')
# print(replace_dict)


# BOOK
book_json_path = os.path.join(folder_path, 'book.json')
dest_book_json_path = os.path.join(dest_folder_path, 'book.json')
if os.path.exists(book_json_path):
    with open(book_json_path) as book_obj:
        book_data = json.load(book_obj)

    for book_record in book_data:
        book_record['event_identity']['object_id'] = str(replace_dict['book'][reference_data_map['book_name'][book_record['event_identity']['object_id']]])

    with open(dest_book_json_path, 'w') as book_obj:
        json.dump(book_data, book_obj, indent=2)


# BUNDLE
bundle_json_path = os.path.join(folder_path, 'bundle.json')
dest_bundle_json_path = os.path.join(dest_folder_path, 'bundle.json')
if os.path.exists(bundle_json_path):
    with open(bundle_json_path) as bundle_obj:
        bundle_data = json.load(bundle_obj)

    for bundle_record in bundle_data:
        bundle_record['event_identity']['object_id'] = str(replace_dict['bundle'][reference_data_map['bundle_id'][bundle_record['event_identity']['object_id']]])

    with open(dest_bundle_json_path, 'w') as bundle_obj:
        json.dump(bundle_data, bundle_obj, indent=2)


# TRADE
trade_json_path = os.path.join(folder_path, 'trades.json')
dest_trade_json_path = os.path.join(dest_folder_path, 'trades.json')
if os.path.exists(trade_json_path):
    with open(trade_json_path) as trade_obj:
        trade_data = json.load(trade_obj)

    for trade_record in trade_data:
        trade_record['transaction']['book_id'] = str(replace_dict['book'][reference_data_map['book_name'][trade_record['transaction']['book_id']]])
        trade_record['transaction']['security_id'] = str(replace_dict['spn'][reference_data_map['spn'][trade_record['transaction']['security_id']]])
        trade_record['transaction']['trade']['custodian_account_id'] = str(replace_dict['ca'][reference_data_map['accountId'][trade_record['transaction']['trade']['custodian_account_id']]])
        trade_record['transaction']['trade']['bundle_id'] = str(replace_dict['bundle'][reference_data_map['bundle_id'][trade_record['transaction']['trade']['bundle_id']]])
        
    with open(dest_trade_json_path, 'w') as trade_obj:
        json.dump(trade_data, trade_obj, indent=2)


# Securities
for sec_file in ['security_equity', 'security_option', 'security_ccy_fwd', 'security_bond', 'security_futures', 'security_swap']:
    sec_json_path = os.path.join(folder_path, sec_file + '.json')
    dest_sec_json_path = os.path.join(dest_folder_path, sec_file + '.json')
    if os.path.exists(sec_json_path):
        with open(sec_json_path) as sec_obj:
            sec_data = json.load(sec_obj)

        for sec_record in sec_data:
            sec_record['event_identity']['object_id'] = str(replace_dict['spn'][reference_data_map['spn'][sec_record['event_identity']['object_id']]])

        with open(dest_sec_json_path, 'w') as sec_obj:
            json.dump(sec_data, sec_obj, indent=2)


# CUSTODIAN_ACCOUNT
ca_json_path = os.path.join(folder_path, 'custodian_account.json')
dest_ca_json_path = os.path.join(dest_folder_path, 'custodian_account.json')
if os.path.exists(ca_json_path):
    with open(ca_json_path) as ca_obj:
        ca_data = json.load(ca_obj)

    for ca_record in ca_data:
        ca_record['event_identity']['object_id'] = str(replace_dict['ca'][reference_data_map['accountId'][ca_record['event_identity']['object_id']]])

    with open(dest_ca_json_path, 'w') as ca_obj:
        json.dump(ca_data, ca_obj, indent=2)


# MAT
mat_json_path = os.path.join(folder_path, 'mat.json')
dest_mat_json_path = os.path.join(dest_folder_path, 'mat.json')
if os.path.exists(mat_json_path):
    with open(mat_json_path) as mat_obj:
        mat_data = json.load(mat_obj)

    for mat_record in mat_data:
        mat_record['transaction']['book_id'] = str(replace_dict['book'][reference_data_map['book_name'][mat_record['transaction']['book_id']]])
        mat_record['transaction']['security_id'] = str(replace_dict['spn'][reference_data_map['spn'][mat_record['transaction']['security_id']]])
        mat_record['transaction']['single_sided_adjustment']['custodian_account_id'] = str(replace_dict['ca'][reference_data_map['accountId'][mat_record['transaction']['single_sided_adjustment']['custodian_account_id']]])
        mat_record['transaction']['single_sided_adjustment']['bundle_id'] = str(replace_dict['bundle'][reference_data_map['bundle_id'][mat_record['transaction']['single_sided_adjustment']['bundle_id']]])

    with open(dest_mat_json_path, 'w') as mat_obj:
        json.dump(mat_data, mat_obj, indent=2)


# RESETS
resets_json_path = os.path.join(folder_path, 'RESETS.json')
dest_resets_json_path = os.path.join(dest_folder_path, 'RESETS.json')
if os.path.exists(resets_json_path):
    with open(resets_json_path) as resets_obj:
        resets_data = json.load(resets_obj)

    for resets_record in resets_data:
        resets_record['transaction']['book_id'] = str(replace_dict['book'][reference_data_map['book_name'][resets_record['transaction']['book_id']]])
        resets_record['transaction']['security_id'] = str(replace_dict['spn'][reference_data_map['spn'][resets_record['transaction']['security_id']]])
        resets_record['transaction']['swap_reset']['custodian_account_id'] = str(replace_dict['ca'][reference_data_map['accountId'][resets_record['transaction']['swap_reset']['custodian_account_id']]])

    with open(dest_resets_json_path, 'w') as reset_obj:
        json.dump(resets_data, reset_obj, indent=2)


# PRICES
prices_json_path = os.path.join(folder_path, 'prices.json')
dest_prices_json_path = os.path.join(dest_folder_path, 'prices.json')
if os.path.exists(prices_json_path):
    with open(prices_json_path) as prices_obj:
        prices_data = json.load(prices_obj)

    for prices_record in prices_data:
        prices_record['price']['record_key'] = str(replace_dict['spn'][reference_data_map['spn'][prices_record['price']['record_key']]])

    with open(dest_prices_json_path, 'w') as prices_obj:
        json.dump(prices_data, prices_obj, indent=2)


# TAX_LOT INITIALIZER
taxlot_json_path = os.path.join(folder_path, 'taxlot_initializer.json')
dest_taxlot_json_path = os.path.join(dest_folder_path, 'taxlot_initializer.json')
if os.path.exists(taxlot_json_path):
    with open(taxlot_json_path) as taxlot_obj:
        taxlot_data = json.load(taxlot_obj)

    for taxlot_record in taxlot_data:
        taxlot_record['transaction']['book_id'] = str(replace_dict['book'][reference_data_map['book_name'][taxlot_record['transaction']['book_id']]])
        taxlot_record['transaction']['security_id'] = str(replace_dict['spn'][reference_data_map['spn'][taxlot_record['transaction']['security_id']]])
        taxlot_record['transaction']['tax_lot_initializer']['custodian_account_id'] = str(replace_dict['ca'][reference_data_map['accountId'][taxlot_record['transaction']['tax_lot_initializer']['custodian_account_id']]])
        taxlot_record['transaction']['tax_lot_initializer']['bundle_id'] = str(replace_dict['bundle'][reference_data_map['bundle_id'][taxlot_record['transaction']['tax_lot_initializer']['bundle_id']]])

    with open(dest_taxlot_json_path, 'w') as taxlot_obj:
        json.dump(taxlot_data, taxlot_obj, indent=2)

print((datetime.now() - t0).total_seconds())
