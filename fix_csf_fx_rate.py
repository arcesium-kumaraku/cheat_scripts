import pandas as pd
import os
import re


base_path = '/home/guptaksh/Documents/TEST_M-20230518-fx-fix'
DIVISION_FACTOR = 120

book_path = os.path.join(base_path, 'scenario-ref-data', 'input', 'book.csv')
book_df = pd.read_csv(book_path, keep_default_na=False, usecols=['book_id', 'book_currency'])
# print(book_df)

lot_initializer_path = os.path.join(base_path, 'lot_initializer_day_1', 'input', 'taxlot_initializer.csv')
if os.path.exists(lot_initializer_path):
    lot_initializer_df = pd.read_csv(lot_initializer_path, keep_default_na=False)
    lot_initializer_df['trade_date_fx_rate'] = lot_initializer_df['trade_date_fx_rate'].div(DIVISION_FACTOR).round(2)

    for book_id, book_currency in zip(book_df['book_id'], book_df['book_currency']):
        lot_initializer_filtered_df = lot_initializer_df[(lot_initializer_df['book_id'] == book_id) & (lot_initializer_df['settle_currency_id'] == book_currency)]
        lot_initializer_filtered_df.loc[lot_initializer_filtered_df.index, 'trade_date_fx_rate'] = 1
        lot_initializer_filtered_df.loc[lot_initializer_filtered_df.index, 'transaction_date_fx_rate'] = ''

        lot_initializer_df.loc[lot_initializer_filtered_df.index, 'trade_date_fx_rate'] = lot_initializer_filtered_df['trade_date_fx_rate']
        lot_initializer_df.loc[lot_initializer_filtered_df.index, 'transaction_date_fx_rate'] = lot_initializer_filtered_df['transaction_date_fx_rate']

    lot_initializer_df.to_csv(lot_initializer_path, index=False)


pattern = r'^random_\d+_btc_1'
random_folders = [folder for folder in os.listdir(base_path) if os.path.isdir(os.path.join(base_path, folder)) and re.match(pattern, folder)]
for folder in sorted(random_folders, key=lambda s: int(s.split('_')[1])):
    random_folder_path = os.path.join(base_path, folder, 'input')

    trade_path = os.path.join(random_folder_path, 'trades.csv')
    if os.path.exists(trade_path):
        trade_df = pd.read_csv(trade_path, keep_default_na=False)
        trade_df['settle_date_fx_rate'] = ''
        trade_df['trade_date_fx_rate'] = trade_df['trade_date_fx_rate'].div(DIVISION_FACTOR).round(2)

        for book_id, book_currency in zip(book_df['book_id'], book_df['book_currency']):
            trade_filtered_df = trade_df[(trade_df['book_id'] == book_id) & (trade_df['settle_currency_id'] == book_currency)]
            trade_filtered_df.loc[trade_filtered_df.index, 'trade_date_fx_rate'] = 1
            
            trade_df.loc[trade_filtered_df.index, 'trade_date_fx_rate'] = trade_filtered_df['trade_date_fx_rate']

        trade_df.to_csv(trade_path, index=False)

    fx_rates_path = os.path.join(random_folder_path, 'fx_rates.csv')
    if os.path.exists(fx_rates_path):
        fx_rates_df = pd.read_csv(fx_rates_path, keep_default_na=False)
        fx_rates_df['fx_rate'] = fx_rates_df['fx_rate'].div(DIVISION_FACTOR).round(2)
        fx_rates_df.to_csv(fx_rates_path, index=False)

    ea_path = os.path.join(random_folder_path, 'exercise_assign.csv')
    if os.path.exists(ea_path):
        ea_df = pd.read_csv(ea_path, keep_default_na=False)
        ea_df['trade_date_fx_rate'] = ea_df['trade_date_fx_rate'].div(DIVISION_FACTOR).round(2)
        ea_df['settle_date_fx_rate'] = ''
        ea_df.to_csv(ea_path, index=False)
