import json
import os
from tqdm import tqdm
import argparse


def add_keys(base_path):
    for folders in tqdm(os.listdir(base_path)):
        # print(folders)
        for folder in os.listdir(os.path.join(base_path, folders)):
            for file in os.listdir(os.path.join(base_path, folders, folder)):
                    if file.__contains__('trades.json'):
                        with open(os.path.join(base_path, folders, folder, file), 'r') as f:
                            data = json.load(f)

                            for n, t_data in enumerate(data):
                                t_data['transaction']['transaction_linked_security_attributes'] = dict()
                                t_data['transaction']['transaction_linked_security_attributes']['birth_date'] = dict()
                                t_data['transaction']['transaction_linked_security_attributes']['birth_date'] = {"year": 1900, "month": 1, "day": 1}

                                t_data['transaction']['transaction_date'] = t_data['transaction']['settle_date']
                                
                                data[n] = t_data

                        with open(os.path.join(base_path, folders, folder, file), 'w') as f:
                            json.dump(data, f, indent=2)


                    if file.__contains__('taxlot_initializer.json'):
                        with open(os.path.join(base_path, folders, folder, file), 'r') as f:
                            data = json.load(f)

                            for n, t_data in enumerate(data):
                                t_data['transaction']['transaction_linked_security_attributes']['underlying_security_id'] = ""
                                t_data['transaction']['transaction_linked_security_attributes']['transaction_currency_id'] = ""
                                t_data['transaction']['transaction_linked_security_attributes']['birth_date'] = {"year": 1900, "month": 1, "day": 1}
                                
                                t_data['transaction']['transaction_date'] = t_data['transaction']['settle_date']
                                
                                data[n] = t_data

                        with open(os.path.join(base_path, folders, folder, file), 'w') as f:
                            json.dump(data, f, indent=2)
                    
                    if file.__contains__('bundle.json'):
                        with open(os.path.join(base_path, folders, folder, file), 'r') as f:
                            data = json.load(f)

                            for n, t_data in enumerate(data):
                                t_data['reference_data']['bundle']['creation_date'] = {"year": 1900, "month": 1, "day": 1}
                                
                                data[n] = t_data

                        with open(os.path.join(base_path, folders, folder, file), 'w') as f:
                            json.dump(data, f, indent=2)
                    
                    if file.__contains__('custodian_account.json'):
                        with open(os.path.join(base_path, folders, folder, file), 'r') as f:
                            data = json.load(f)

                            for n, t_data in enumerate(data):                            
                                t_data['reference_data']['custodian_account']['creation_date'] = {"year": 1900, "month": 1, "day": 1}
                                
                                data[n] = t_data

                        with open(os.path.join(base_path, folders, folder, file), 'w') as f:
                            json.dump(data, f, indent=2)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--base-path', dest='base_path', required=True)
    args = parser.parse_args()

    if os.path.exists(args.base_path):
        add_keys(args.base_path)


if __name__ == '__main__':
    '''
    python3 add_keys_proto_json.py --base-path /home/guptaksh/Documents/JSON_TEST_SHORT-24_final_v6
    '''

    main()
