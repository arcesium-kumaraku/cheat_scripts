import pandas as pd
import os
import sys
import shutil
import time

t0 = time.time()

base_path = '/home/guptaksh/Downloads/'
dest_dataset_path = os.path.join(base_path, 'csf-book-tagged-data/TEST_SHORT')

if os.path.exists(dest_dataset_path):
    shutil.rmtree(dest_dataset_path, ignore_errors=True)
os.makedirs(dest_dataset_path)

dataset_combination = {'XS': [os.path.join(base_path, 'csf-book-tagged-data/TEST_XS'), 20],
                       'S': [os.path.join(base_path, 'csf-book-tagged-data/TEST_S'), 4]}

for key, value in dataset_combination.items():
    for index in range(1, value[-1]+1):
        dest_path = os.path.join(dest_dataset_path, f'{key}_{index}')
        if os.path.exists(value[0]):
            shutil.copytree(value[0], dest_path)

for dataset_path in os.listdir(dest_dataset_path):
    for day_wise_folder in [dir for dir in os.listdir(os.path.join(dest_dataset_path, dataset_path)) if os.path.isdir(os.path.join(dest_dataset_path, dataset_path, dir))]:
        path = os.path.join(dest_dataset_path, dataset_path, day_wise_folder, 'input')
        list_files = os.listdir(path)
        list_files.remove('configuration.json')
        for file in list_files:
            df = pd.read_csv(os.path.join(path, file), keep_default_na=False)
            if 'book_id' in df.columns:
                df['book_id'] = f'BOOK_{dataset_path}'

            if file == 'book.csv':
                for column in ['abbrev', 'client_abbrev', 'book_name']:
                    df[column] = f'BOOK_{dataset_path}'

            df.to_csv(os.path.join(path, file), index=False)

print('Time taken: ', time.time() - t0)
