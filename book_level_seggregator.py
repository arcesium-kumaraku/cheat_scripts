import os
import pandas as pd
import time
import json
import shutil


t0 = time.time()

base_path = '/home/guptaksh/code/test-data-provider/test_data/test_scenarios/MGP_Consolidated_test_cases/amend_2/input'


def normalize_reorg_destination_spn(folder_path):
    df = pd.read_csv(os.path.join(folder_path, 'reorgs.csv'), keep_default_na=False, dtype=str)

    reorg_legs = [col for col in df.columns if 'reorg_leg_' in col]

    tmp_reorg_dict = dict()
    for reorg_leg_i in range(0, len(reorg_legs)):
        tmp_reorg_dict[f'reorg_leg_{reorg_leg_i}'] = [t for t in df[f'reorg_leg_{reorg_leg_i}'].str.split(';')]

    tmp_reorg_dict_1 = dict()
    for k, v in tmp_reorg_dict.items():
        tmp_reorg_dict_1[k] = dict()
        tmp_reorg_list = list()
        for i in range(0, len(v)):
            tmp_reorg_dict_2 = dict()
            for j in range(0, len(v[i])):
                if ':' in v[i][j]:
                    tmp_reorg_dict_2.update({v[i][j].split(':')[0]: v[i][j].split(':')[1]})
            tmp_reorg_list.append(tmp_reorg_dict_2)
        tmp_reorg_dict_1[k] = tmp_reorg_list

    leg_columns = list()
    for reorg_leg in range(1, len(reorg_legs) + 1):
        leg_columns.extend([f'Reorg_Leg_Destination SPN_{reorg_leg}'])

    dest_df = pd.DataFrame(columns=['book_id', 'spn', *leg_columns])

    dest_df['book_id'] = df['book_id'].values
    dest_df['spn'] = df['spn'].values

    for t_k, t_v in tmp_reorg_dict_1.items():
        reorg_leg_1 = int(t_k.rpartition('_')[-1]) + 1
        for t_v_i in range(0, len(t_v)):
            if 'destination_spn' in t_v[t_v_i]:
                dest_df[f'Reorg_Leg_Destination SPN_{reorg_leg_1}'][t_v_i] = t_v[t_v_i]['destination_spn']
            else:
                dest_df[f'Reorg_Leg_Destination SPN_{reorg_leg_1}'][t_v_i] = ''

    dest_df.to_csv(os.path.join(folder_path, 'reorgs.csv'), index=False)


def write_files(book_mapping, security_files, txn_files, s_dfs, t_dfs, blind_t_dfs):
    dest_path = os.path.join(base_path, 'book_level_data')
    if os.path.exists(dest_path):
        shutil.rmtree(dest_path)
    os.mkdir(dest_path)

    blind_copy_files = ['configuration.json', 'tax_status.csv', 'tax_matrix.csv', 'bundle.csv', 'custodian_account.csv',
                        'price_group.csv', 'paydowns.csv', 'organization.csv', 'market.csv', 'global_activity.csv',
                        'credit_activity.csv', 'country.csv', 'cash_dividends.csv', 'calendar.csv', 'business_unit.csv',
                        'agreement.csv', 'withholding_rule.csv', 'splits.csv', 'fx_rates.csv', 'security_currency.csv']

    book_path = os.path.join(base_path, 'book.csv')
    is_book_csv_exists = False
    if os.path.exists(os.path.join(base_path, 'book.csv')):
        is_book_csv_exists = True
        book_df = pd.read_csv(book_path, keep_default_na=False)

    missing_book_record = list()

    dfs = dict()
    for d_file in ['prices', 'vrs', 'fwd_rates']:
        d_file_path = os.path.join(base_path, d_file + '.csv')
        if os.path.exists(d_file_path):
            dfs[d_file] = pd.read_csv(d_file_path, keep_default_na=False)

    for book, fields in book_mapping.items():
        book_wise_dest_path = os.path.join(dest_path, book, 'input')
        os.makedirs(book_wise_dest_path)
        if is_book_csv_exists:
            df = book_df[book_df['book_id'].isin([book])]

            if len(df) > 0:
                df.to_csv(os.path.join(book_wise_dest_path, 'book.csv'), index=False)
            else:
                missing_book_record.append(book)

        for copy_file in blind_copy_files:
            if os.path.exists(os.path.join(base_path, copy_file)):
                shutil.copy(os.path.join(base_path, copy_file), os.path.join(book_wise_dest_path, copy_file))
        shutil.copy(os.path.join(base_path, '..', 'manifest.json'), os.path.join(book_wise_dest_path, '..', 'manifest.json'))

        for security_file in security_files:
            spn_field = 'facility_spn' if security_file == 'security_credit_contract.csv' else 'spn'
            df = s_dfs[security_file][s_dfs[security_file][spn_field].isin(fields['spn'])]
            if len(df) > 0:
                security_file_path = os.path.join(book_wise_dest_path, security_file)
                if os.path.exists(security_file_path):
                    temp_security_df = pd.read_csv(security_file_path, keep_default_na=False)
                    df = df.append(temp_security_df)
                df.to_csv(security_file_path, index=False)

        for t_file in txn_files:
            if t_file in t_dfs:
                if 'spn' in t_dfs[t_file].columns:
                    df = t_dfs[t_file][t_dfs[t_file]['spn'].isin(fields['spn'])]
                    df = df[df['book_id'].isin([book])]
                    if len(df) > 0:
                        df.to_csv(os.path.join(book_wise_dest_path, t_file), index=False)
                else:
                    df = t_dfs[t_file][t_dfs[t_file]['book_id'].isin([book])]
                    if len(df) > 0:
                        df.to_csv(os.path.join(book_wise_dest_path, t_file), index=False)

        for d_file in ['prices', 'vrs', 'fwd_rates']:
            if d_file in dfs:
                df = dfs[d_file][dfs[d_file]['spn'].isin(fields['spn'])]
                if len(df) > 0:
                    df.to_csv(os.path.join(book_wise_dest_path, d_file + '.csv'), index=False)

        write_blind_copy_txn_spn(book_wise_dest_path, security_files, s_dfs, blind_t_dfs)
    print(f'Missing book in book.csv: {missing_book_record}')


def write_blind_copy_txn_spn(book_wise_dest_path, security_files, s_dfs, blind_t_dfs):
    for txn_file in ['paydowns.csv', 'cash_dividends.csv', 'splits.csv']:
        if os.path.exists(os.path.join(base_path, txn_file)):
            for security_file in security_files:
                spn_field = 'facility_spn' if security_file == 'security_credit_contract.csv' else 'spn'
                df = s_dfs[security_file][s_dfs[security_file][spn_field].isin(blind_t_dfs[txn_file]['spn'].unique())]
                if len(df) > 0:
                    security_file_path = os.path.join(book_wise_dest_path, security_file)
                    if os.path.exists(security_file_path):
                        temp_security_df = pd.read_csv(security_file_path, keep_default_na=False)
                        df = df.append(temp_security_df)
                    df.to_csv(security_file_path, index=False)


# Book_id to SPN mapping
txn_file_names = ['trades.csv', 'resets.csv', 'transfer.csv', 'reorgs.csv', 'mat.csv', 'ccy_fwd.csv', 'taxlot_initializer.csv', 'cost_qty_adjust.csv', 'spots.csv']
book_id_fields_map = dict()
spn_fields = {'spn': ['spn', 'Reorg_Leg_Destination SPN_1', 'Reorg_Leg_Destination SPN_2', 'Reorg_Leg_Destination SPN_3', 'Reorg_Leg_Destination SPN_4']}
book_txn_spn_map = dict()
t_dfs = dict()

for file in txn_file_names:
    file_path = os.path.join(base_path, file)
    if os.path.exists(file_path):
        t_dfs[file] = pd.read_csv(file_path, keep_default_na=False)
        if file == 'reorgs.csv':
            normalize_reorg_destination_spn(base_path)
        t_df = pd.read_csv(file_path, keep_default_na=False)
        a = t_df.groupby('book_id')

        for k, v in spn_fields.items():
            for l in v:
                if l in t_df.columns:
                    data = a[l].apply(set).apply(list).to_dict()

                    for k1, v1 in data.items():
                        if k1 not in book_txn_spn_map:
                            book_txn_spn_map[k1] = dict()
                        if file not in book_txn_spn_map[k1]:
                            book_txn_spn_map[k1][file] = list()
                        if k1 not in book_id_fields_map.keys():
                            book_id_fields_map[k1] = dict()
                        if k not in book_id_fields_map[k1]:
                            book_id_fields_map[k1][k] = list()
                        for t_v in list(set(v1)):
                            if t_v not in book_id_fields_map[k1][k] and t_v != '':
                                book_id_fields_map[k1][k].append(t_v)
                            if t_v not in book_txn_spn_map[k1][file] and t_v != '':
                                book_txn_spn_map[k1][file].append(t_v)

security_files = [file for file in os.listdir(base_path) if file.startswith('security_')]
try:
    security_files.remove('security_currency.csv')
except:
    pass

s_dfs = dict()
security_dependency_map = dict()
for security_file in security_files:
    t_df = pd.read_csv(os.path.join(base_path, security_file), keep_default_na=False)
    s_dfs[security_file] = t_df
    if 'underlying' in t_df.columns:
        security_dependency_map.update(t_df.groupby('spn')['underlying'].apply(set).apply(list).to_dict())

blind_t_dfs = dict()
for txn_file in ['paydowns.csv', 'cash_dividends.csv', 'splits.csv']:
    if os.path.exists(os.path.join(base_path, txn_file)):
        blind_t_dfs[txn_file] = pd.read_csv(os.path.join(base_path, txn_file), keep_default_na=False)

for k1, v1 in book_id_fields_map.items():
    for k2, v2 in v1.items():
        for value in v1[k2]:
            if value in list(security_dependency_map.keys()):
                book_id_fields_map[k1]['spn'].extend(security_dependency_map[value])

with open(os.path.join(base_path, '..', "book_level_mapping.json"), "w") as m_f:
    json.dump(book_id_fields_map, m_f, indent=2)

with open(os.path.join(base_path, '..', "book_level_txn_spns.json"), "w") as m_f:
    json.dump(book_txn_spn_map, m_f, indent=2)

write_files(book_id_fields_map, security_files, txn_file_names, s_dfs, t_dfs, blind_t_dfs)

print('Time taken: ', time.time() - t0)
