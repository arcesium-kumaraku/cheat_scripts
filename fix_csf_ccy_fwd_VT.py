import pandas as pd
import os
from datetime import datetime


base_path = '/home/guptaksh/Documents/TEST_M-20230518-fx-fix'
start_base_vt = datetime.strptime('20200101', "%Y%m%d")

folder_list = os.listdir(base_path)
random_folder_list = [i for i in folder_list if os.path.isdir(os.path.join(base_path, i))]
random_folder_list.remove('lot_initializer_day_1')
random_folder_list.remove('scenario-ref-data')
random_folder_list.remove('scenario-ref-data-new')

for folder in sorted(random_folder_list, key=lambda s: int(s.split('_')[1])):
    folder_path = os.path.join(base_path, folder, 'input')
    folder_day = int(folder.split('_')[1])
    for file in [j for j in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, j)) and j.endswith('ccy_fwd.csv')]:
        file_path = os.path.join(folder_path, file)
        if os.path.exists(file_path):
            df = pd.read_csv(file_path, keep_default_na=False)
            os.remove(file_path)
            for start_vt in list(set([i.split(' ')[0] for i in sorted(df['start_valid_time'])])):
                day = (datetime.strptime(start_vt, '%Y%m%d') - start_base_vt).days + 1
                dest_file_path = os.path.join(base_path, f'random_{day}_btc_1', 'input', file)
                if os.path.exists(dest_file_path):
                    df2 = pd.read_csv(dest_file_path, keep_default_na=False)
                    df = df.append(df2)
                df['start_knowledge_time'] = min(df['start_knowledge_time'])
                df.to_csv(dest_file_path, index=False)
