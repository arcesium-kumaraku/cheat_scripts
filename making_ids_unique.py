import pandas as pd
import os
import time

t0 = time.time()

base_path = '/home/guptaksh/Documents/pnl_coordination_data'
folder_name = 'TEST_XXS'

field_map = {'taxlot_initializer.csv': ['transaction_id', 'spn'], 'fwd_rates.csv': ['record_id', 'spn'], 'fx_rates.csv': ['fx_record_id'], 'mat.csv': ['transaction_id', 'spn'], 'prices.csv': ['record_id', 'spn'], 'trades.csv': ['trade_id', 'spn'], 'security_bond.csv': ['spn', 'name'], 'security_ccy_fwd.csv': ['spn', 'name'], 'security_equity.csv': ['spn', 'name'], 'security_futures.csv': ['spn', 'name'], 'security_linker.csv': ['spn', 'name', 'inflation_ratio_id'], 'security_option.csv': ['spn', 'name', 'underlying'], 'security_swap.csv': ['spn', 'name', 'underlying'], 'inflation_rates.csv': ['record_id', 'inflation_ratio_id'], 'reorgs.csv': ['transaction_id', 'spn'], 'vrs.csv': ['record_id', 'spn']}

folders = [i for i in os.listdir(os.path.join(base_path, folder_name)) if os.path.isdir(os.path.join(base_path, folder_name, i))]
for folder in folders:
    print(folder)
    book_level_folder = [i for i in os.listdir(os.path.join(base_path, folder_name, folder)) if os.path.isdir(os.path.join(base_path, folder_name, folder, i))]
    for day_level in book_level_folder:
        for file, columns in field_map.items():
            for column in columns:
                file_path = os.path.join(base_path, folder_name, folder, day_level, 'input', file)
                if os.path.exists(file_path):
                    df = pd.read_csv(file_path, keep_default_na=False)
                    if column in df.columns:
                        df[column] = df[column] + '_' + folder
                    df.to_csv(file_path, index=False)

print('Time taken: ', time.time() - t0)
