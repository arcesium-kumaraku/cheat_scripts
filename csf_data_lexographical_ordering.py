import os
import argparse


def lexographical_ordering(base_path):
    folders = [i for i in os.listdir(base_path) if os.path.isdir(os.path.join(base_path, i))]

    if 'lot_initializer_day_1' in folders:
        os.rename(os.path.join(base_path, 'lot_initializer_day_1'), os.path.join(base_path, 'b_lot_initializer_day_1'))
        folders.remove('lot_initializer_day_1')

    if 'scenario-ref-data' in folders:
        os.rename(os.path.join(base_path, 'scenario-ref-data'), os.path.join(base_path, 'a_scenario-ref-data'))
        folders.remove('scenario-ref-data')

    if 'scenario-ref-data-new' in folders:
        os.rename(os.path.join(base_path, 'scenario-ref-data-new'), os.path.join(base_path, 'a_scenario-ref-data-new'))
        folders.remove('scenario-ref-data-new')


    for folder in folders:
        folder_split = folder.split('_')
        folder_split[1] = folder_split[1].zfill(3)
        tmp_folder_name = '_'.join(folder_split)

        os.rename(os.path.join(base_path, folder), os.path.join(base_path, tmp_folder_name))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--base-path', dest='base_path', required=True)
    args = parser.parse_args()

    if os.path.exists(args.base_path):
        lexographical_ordering(args.base_path)


if __name__ == '__main__':
    '''
    python3 csf_data_lexographical_ordering.py --base-path /home/guptaksh/Documents/TS_amends/XS/TEST_XS_CSV
    '''

    main()