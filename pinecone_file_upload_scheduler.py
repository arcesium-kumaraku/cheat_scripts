from requests_kerberos import HTTPKerberosAuth, OPTIONAL
import requests


for i in range(34):
    url = 'http://pinecone.europa.c.ia55.net/service/tradeImportService/importFromAffFile'
    file = f'/home/guptaksh/code/test-data-provider/trinity_to_prt/csv_grande/trades/{i}_trades.csv'
    try:
        r = requests.post(url, files={'file': open(file, 'rb')}, data={}, auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL))
        print(r)
    except:
        print("Exception occurred..")
