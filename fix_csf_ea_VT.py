import pandas as pd
import os
from datetime import datetime
import numpy as np


base_path = '/home/guptaksh/Documents/TEST_M-20230503'

folder_list_all = os.listdir(base_path)
folder_list = [i for i in folder_list_all if os.path.isdir(os.path.join(base_path, i))]
folder_list.remove('lot_initializer_day_1')
folder_list.remove('scenario-ref-data')
folder_list.remove('scenario-ref-data-new')


df = pd.DataFrame(columns=["transaction_id", "book_id", "spn", "bundle_id", "quantity", "price", "underlying_spn", "ea_type", "underlying_quantity", "operation_code", "trade_date", "settle_date", "actual_settle_date", "ca_id", "settle_currency_id", "settle_fx_rate", "trade_date_fx_rate", "settle_date_fx_rate", "impact_level", "comments", "commission", "commission_ccy", "commission_specify_date", "exchange_fees", "exchange_fees_ccy", "exchange_fees_specify_date", "tax", "tax_ccy", "tax_specify_date", "ticket_charges", "ticket_charges_ccy", "ticket_charges_specify_date", "clearing_fees", "clearing_fees_ccy", "clearing_fees_specify_date", "nfa_fees", "nfa_fees_ccy", "nfa_fees_specify_date", "start_knowledge_time", "start_valid_time"])

for i in folder_list:
    folder_path = os.path.join(base_path, i, 'input')
    file_path = os.path.join(folder_path, 'exercise_assign.csv')
    if os.path.exists(file_path):
        t_df = pd.read_csv(file_path, keep_default_na=False)
        os.remove(file_path)
        t_df = t_df[~t_df['start_valid_time'].str.contains('2019')]
        df = df.append(t_df)

df.to_csv(os.path.join(base_path, 'exercise_assign.csv'), index=False)

print("Segregating records in exercise_assign.csv based on VT")
start_base_vt = datetime.strptime('20200101', "%Y%m%d")
ea_path = os.path.join(base_path, 'exercise_assign.csv')
if os.path.exists(ea_path):
    df_ea = pd.read_csv(ea_path, keep_default_na=False)
    print(f"Initial exercise_assign records count: {len(df_ea.index)}\n")
    df_ea.replace([np.nan, r'^\s+$'], '', regex=True, inplace=True)
    start_vts = [i.split(' ')[0] for i in sorted(df_ea['start_valid_time'].unique())]
    total_final_len = 0
    for start_vt in start_vts:
        day = (datetime.strptime(start_vt, '%Y%m%d') - start_base_vt).days + 1
        print(f"{start_vt} -- Day: {day}")
        df1 = df_ea[df_ea['start_valid_time'].str.contains(start_vt)]
        total_final_len = total_final_len + len(df1.index)
        dest_path = os.path.join(base_path, f'random_{day}_btc_1', 'input')
        df1.to_csv(os.path.join(dest_path, 'exercise_assign.csv'), index=False)

    print(f"\nFinal count after segregation: {total_final_len}")
    os.remove(ea_path)