import requests
from requests_kerberos import HTTPKerberosAuth, OPTIONAL
import argparse
import time
import os
import json
from datetime import datetime
import sys


def get_args_parser():
    parser = argparse.ArgumentParser(description="Utility to upload data file")
    parser.add_argument("--dataset-path", dest="dataset_path", action="store", default=os.getcwd())
    parser.add_argument("--book_path", dest="book_path", action="store", default=os.getcwd())
    parser.add_argument("--client-name", dest="client_name",action="store", required=True)

    return parser


def push_data_file(dataset_path, json_list):
    dataset_s3_url_dict = dict()
    for i_json in json_list:
        file = os.path.join(dataset_path, i_json)
        try:
            url = f'https://nautilus-d2a-connector.{args.client_name}.c.ia55.net/pushMockDataController/pushDataFile'
            r = requests.post(url, files={'dataFile': open(file, 'rb')}, auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL))
            dataset_s3_url_dict[i_json.rpartition('.')[0]] = r.text
            print(i_json, r.text)
        except Exception as e:
            print(f"Exception occurred. {e}")

    return dataset_s3_url_dict


def get_book_id(dataset_path):
    book_id = ''
    if 'scenario-ref-data' in dataset_path:
        file_name = 'book.json'
        nest1, nest2 = 'event_identity', 'object_id'
    else:
        file_name = 'trades.json'
        nest1, nest2 = 'transaction', 'book_id'
    
    json_file_path = os.path.join(dataset_path, file_name)
    if os.path.exists(json_file_path):
        with open(json_file_path) as json_obj:
            json_file = json.load(json_obj)
        book_id = json_file[0][nest1][nest2]
    else:
        sys.exit(f"File: {json_file_path} doesn't exist.")
    
    return book_id


def bulk_push_mock_data(dataset_path, s3_urls):
    book_id = get_book_id(dataset_path)

    final_list = list()

    data_type_case_dict = {'REFERENCE_DATA_TYPE': ['BROKER', 'book'], 'TRANSACTION': ['DEFAULT', 'resets', 'mat', 'dividend', 'tax_lot_closing', 'tax_lot_initializer', 'COLLAPSE_BOX', 'EXERCISE_ASSIGN', 'DEPOSIT_WITHDRAWAL', 'LOAN_ACTIVITY', 'swap_resets', 'CUSTODIAN_LOCATION', 'REALIZED_BUNDLE_TRANSFER', 'REPO_REVERSE_REPO', 'fx_forward', 'SPOT_FX', 'INTEREST', 'PAYDOWN', 'REORGANIZATION', 'SPLIT_TRANSACTION', 'trades', 'TRANSFER', 'EXPIRE', 'MATURE', 'COST_AND_QUANTITY_ADJUSTMENT', 'DOUBLE_SIDED_ADJUSTMENT', 'SINGLE_SIDED_ADJUSTMENT', 'TRANSFER_ADJUSTMENT', 'RETURN_OF_CAPITAL', 'REPO_BUNDLE_TRANSFER'], 'SECURITY_TYPE': ['security_option', 'security_ccy_fwd', 'security_equity', 'security_bond', 'CREDIT_DEFAULT_SWAP', 'BANK_DEBT', 'security_currency', 'security_swap', 'INTEREST_RATE_SWAP', 'LISTED_OPTION', 'OTC_OPTION', 'security_futures', 'ENTITLEMENT', 'FINANCING', 'EXOTIC_SWAP', 'CURRENCY_FORWARD', 'FORWARD_AGREEMENT', 'TOTAL_RETURN_SWAP', 'COLLATERAL_BACKED_SECURITY', 'INDEX', 'COIN'], 'REFERENCE_DATA_TYPE': ['BUSINESS_UNIT', 'bundle', 'CUSTODIAN', 'custodian_account', 'LEGAL_ENTITY', 'book', 'BROKER', 'PRICE_LIST', 'PRICE_MARKET', 'PRICE_PROVIDER', 'PRICE_SCHEDULE', 'PRICE_SOURCE', 'PRICE_TYPE', 'COUNTRY', 'MARKET', 'CALENDAR', 'BUSINESS_YEAR_CALENDAR', 'FINANCIAL_COMPONENT', 'WITHHOLDING_TAX_TYPE', 'TAX_STATUS', 'SECURITY_TYPE', 'SECURITY_SUBTYPE', 'FISCAL_CALENDAR', 'HOLDING_PERIOD_TAX_RATES', 'AGREEMENT', 'BUNDLE_GROUP', 'ORGANIZATION', 'COMPANY_MASTER'], 'DERIVED_DATA_TYPE': ['FINANCIAL_COMPONENT_BALANCE', 'OPEN_TAX_LOT', 'CLOSED_TAX_LOT', 'JOURNAL_ENTRY', 'PNL_SUMMARY', 'POSITION_SUMMARY', 'CASH_ACTIVITY', 'DIVIDEND_ACTIVITY', 'INTEREST_ACTIVITY', 'GLOBAL_EVENTS', 'ACCOUNTING_EXCEPTION'], 'SCHEDULE_TYPE': ['RATE_SCHEDULE', 'COUPON_SCHEDULE', 'WITHHOLDING_SCHEDULE', 'RATE_HISTORY', 'COUPON_HISTORY', 'WITHHOLDING_HISTORY', 'CALL_PUT_SCHEDULE', 'FACTOR_SCHEDULE', 'AMORTIZATION_SCHEDULE'], 'CORPORATE_ACTION_TYPE': ['SPLIT', 'CASH_DIVIDEND', 'MERGER_ACQUISITION', 'RIGHTS_ISSUE', 'SHARE_RECLASSIFICATION', 'SPIN_OFF', 'RETURN_OF_CAPITAL'], 'PRICE_TYPE': ['prices', 'fx_rates', 'fwd_rates']}

    data_type_dict = {'broker': 'REF_DATA_BROKER', 'book': 'REF_DATA_BOOK', 'agreement': 'REF_DATA_AGREEMENT', 'security_equity': 'SECURITY_TYPE_EQUITY', 'LEGAL_ENTITY': 'REF_DATA_LEGAL_ENTITY', 'custodian_account': 'REF_DATA_CUSTODIAN_ACCOUNT', 'trades': 'TRANSACTION_TYPE_TRADE', 'bundle': 'REF_DATA_TYPE_BUNDLE',  'fx_rates': 'PRICE_TYPE_SPOT_RATE', 'mat': 'TRANSACTION_TYPE_SINGLE_SIDED_ADJUSTMENT', 'prices': 'PRICE_TYPE_EOD', 'resets': 'TRANSACTION_TYPE_SWAP_RESET', 'security_bond': 'SECURITY_TYPE_BOND', 'security_ccy_fwd': 'SECURITY_TYPE_CURRENCY_FORWARD', 'security_currency': 'SECURITY_TYPE_CURRENCY', 'security_futures': 'SECURITY_TYPE_FUTURE', 'security_option': 'SECURITY_TYPE_LISTED_OPTION', 'security_swap': 'SECURITY_TYPE_TOTAL_RETURN_SWAP', 'fwd_rates': 'PRICE_TYPE_FORWARD_RATE'}

    for dataset, s3_url in s3_urls.items():
        dataset_obj = dict()

        for key, value in data_type_case_dict.items():
            if dataset in value:
                dataset_obj['dataTypeCase'] = key
                break

        time.sleep(1)
        dataset_obj['s3Url'] = s3_url
        dataset_obj['book'] = book_id
        dataset_obj['dataType'] = data_type_dict[dataset]
        # dataset_obj['newWatermark'] = str(int(time.time()*1000))
    
        final_list.append(dataset_obj)	
    
    watermark = str(int(time.time()*1000))
    for t_dict in final_list:
        t_dict['newWatermark'] = watermark
    
    # print(final_list)
    
    try:
        url = f'https://nautilus-d2a-connector.{args.client_name}.c.ia55.net/pushMockDataController/bulkPushMockData'  	
        print(url)
        prepared_json = json.loads(str(final_list).replace("'",'"'))
        print(prepared_json)
        r = requests.post(url, json=prepared_json, auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL))
        print(f"/bulkPushMockData status code: {r.status_code}\n")
        if r.status_code == 200:
            print("done")
        else:
            print(r.text)
    except Exception as e:
        print(f"Exception occurred. {e}")


def fetch_watermarks(books):
    payload = dict()
    payload['books'] = [books]

    print(payload)
    try:
        url = f'https://nautilus-d2a-connector.{args.client_name}.c.ia55.net/p2aWatermarkController/queryP2AWatermarksAvailable'
        r = requests.post(url, json=payload, auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL))
        print(f"/queryP2AWatermarksAvailable status code: {r.status_code}\n")
        print(f'Watermark output: {r.text}')
    except Exception as e:
        print(f"Exception occurred. {e}")


if __name__ == '__main__':
    '''
    python3 upload_data_file_d2a.py --dataset-path /home/guptaksh/Documents/JSON_TEST_SHORT-24_final_v3/S_1/scenario-ref-data --client-name europa
    '''

    args = get_args_parser().parse_args()

    t0 = datetime.now()

    # for n, folder in enumerate(sorted(os.listdir(args.dataset_path), key=int)):
    #     dataset_path = os.path.join(args.dataset_path, folder)
    #     print(f'Uploading folder: {dataset_path}\n')
    #     json_list = [file for file in os.listdir(dataset_path) if file.endswith('.json')]
    #     s3_urls = push_data_file(dataset_path, json_list)
    #     print(f"S3 URLs: {s3_urls}\n")
    #     bulk_push_mock_data(dataset_path, s3_urls)

    print(args.dataset_path)
    json_list = [file for file in os.listdir(args.dataset_path) if file.endswith('.json')]
    s3_urls = push_data_file(args.dataset_path, json_list)
    print(f"S3 URLs: {s3_urls}\n")
    bulk_push_mock_data(args.dataset_path, s3_urls)

    print(f'Time taken: {(datetime.now() - t0).total_seconds()} secs')
