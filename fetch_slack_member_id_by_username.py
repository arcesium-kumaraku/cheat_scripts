from slack_sdk import WebClient
from slack_sdk.http_retry.builtin_handlers import RateLimitErrorRetryHandler


def fetch_slack_member_id_by_username(token, username):
    rate_limit_handler = RateLimitErrorRetryHandler(max_retry_count=3)
    client = WebClient(token=token)
    client.retry_handlers.append(rate_limit_handler)

    cursor = None
    while True:
        response = client.users_list(limit=1000, cursor=cursor)

        if not response['ok']:
            raise Exception('error')

        for x in response['members']:
            if x['name'] == username:
                return x['id']

        cursor = response['response_metadata']['next_cursor']

        if not cursor:
            break


if __name__ == '__main__':
    username = 'guptaksh'
    member_id = fetch_slack_member_id_by_username('#TOKEN#', username)
    print(f'Slack member_id of user: {username}:: {member_id}')
