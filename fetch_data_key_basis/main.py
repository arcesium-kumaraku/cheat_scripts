from yaml.loader import SafeLoader
import yaml
import os
import sys
import argparse
import logging
import pandas as pd
from random import randint
import shutil
from datetime import datetime


def init_logger(level='INFO'):
    root = logging.getLogger()
    root.setLevel(getattr(logging, level.upper()))

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(getattr(logging, level.upper()))
    formatter = logging.Formatter('[%(asctime)s - %(name)s - %(levelname)s] | %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)

    return root


def get_args_parser():
    parser = argparse.ArgumentParser(description="Utility to fetch specific data from dataset")
    parser.add_argument("--data-path", dest="data_path", action="store")
    return parser


def get_unique_folder_name(folder_name):
    is_unique_folder_name = True
    while is_unique_folder_name:
        if os.path.exists(folder_name):
            folder_name = f"{folder_name}_{randint(1, 99)}"
        else:
            is_unique_folder_name = False
    
    return folder_name


def fetch_records_based_on_keys(file_path, keys):
    df = pd.read_csv(file_path, keep_default_na=False)
    
    for key in keys:
        df = df[df[key].isin(config['column'][key])]

    if len(df) > 0:
        modified_file_path = os.path.join(transformed_folder_path, file_path.rpartition(os.path.sep)[-1])
        df.to_csv(modified_file_path, index=False)
    # else:
    #     log.info(f"Not writing {file_path.rpartition(os.path.sep)[-1]}")


if __name__ == '__main__':
    args = get_args_parser().parse_args()
    log = init_logger()

    start_time = datetime.now()

    if not os.path.exists(args.data_path):
        sys.exit(f"Provided data path: '{args.data_path}' doesn't exists")

    with open(os.path.join(os.getcwd(), 'config.yaml')) as f:
        config = yaml.load(f, Loader=SafeLoader)
    
    log.info(f"Config: {config}")

    columns = dict()
    list_files = [file for file in os.listdir(args.data_path) if file.endswith('.csv')]

    for key in config['column'].keys():
        for file in list_files:
            if key in list(pd.read_csv(os.path.join(args.data_path, file), nrows =1)):
                if file not in columns:
                    columns[file] = list()
                columns[file].append(key)
    
    transformed_folder_path = get_unique_folder_name(args.data_path + '_transformed')
    if not os.path.exists(transformed_folder_path):
            os.mkdir(transformed_folder_path)

    for file, keys in columns.items():
        fetch_records_based_on_keys(os.path.join(args.data_path, file), keys)

    for file in config['refData']:
        if os.path.exists(os.path.join(args.data_path, file)):
            shutil.copy(os.path.join(args.data_path, file), os.path.join(transformed_folder_path, file))

    log.info(f'End-to-end time taken: {(datetime.now() - start_time).total_seconds()} secs')
