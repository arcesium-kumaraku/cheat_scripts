import pandas as pd
import time
import os
import multiprocessing as mp


field_map = {
    'taxlot_initializer.csv': ['transaction_id', 'spn'],
    'fwd_rates.csv': ['record_id', 'spn'],
    'fx_rates.csv': ['fx_record_id'],
    'mat.csv': ['transaction_id', 'spn'],
    'prices.csv': ['record_id', 'spn'],
    'trades.csv': ['trade_id', 'spn'],
    'security_bond.csv': ['spn', 'name'],
    'security_ccy_fwd.csv': ['spn', 'name'],
    'security_equity.csv': ['spn', 'name'],
    'security_futures.csv': ['spn', 'name'],
    'security_linker.csv': ['spn', 'name', 'inflation_ratio_id'],
    'security_option.csv': ['spn', 'name', 'underlying', 'deliverable'],
    'security_swap.csv': ['spn', 'name', 'underlying'],
    'inflation_rates.csv': ['record_id', 'inflation_ratio_id'],
    'reorgs.csv': ['transaction_id', 'spn'],
    'vrs.csv': ['record_id', 'spn'],
    'book.csv': ['abbrev', 'client_abbrev', 'book_name'],
    'transfer.csv': ['transaction_id', 'spn', 'source_book_id', 'destination_book_id'],
    'resets.csv': ['spn', 'reset_id']
}


def process_folder(folders_and_files):
    for folder, files in folders_and_files.items():
        print(f'Processing folder: {folder}')
        suffix = folder.split(os.path.sep)[-3]

        for file_name in files:
            if file_name in field_map:
                file_path = os.path.join(folder, file_name)
                df = pd.read_csv(file_path, keep_default_na=False)
                for column in [*field_map[file_name], 'book_id']:
                    if column in df.columns:
                        df[column] = df[column].astype(str).apply(lambda x: x + '_' + suffix)
                df.to_csv(file_path, index=False)


def process_folders_in_parallel(folders_and_files):
    with mp.Pool(processes=mp.cpu_count()) as pool:
        pool.map(process_folder, folders_and_files)
        pool.close()
        pool.join()


if __name__ == '__main__':
    t0 = time.time()

    base_path = '/tmp/pnl_pdf/csv_final_dataset_1'

    folders_and_files = [{root: [file for file in files if file.endswith('.csv')]} for root, dirs, files in os.walk(base_path) if 'input' in root]
    process_folders_in_parallel(folders_and_files)

    print('Time taken:', time.time() - t0)
