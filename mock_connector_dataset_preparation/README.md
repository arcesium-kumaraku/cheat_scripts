# Steps to prepare load test dataset in proto JSON format
## Pre-requisites:
1. Raw fixed CSV

# Steps:
1. Make copies as per requirement - `make_copies.py`
    - Update variables: `base_path` & `dataset_combination`
2. Add suffix to all columns which has to be made unique - `making_ids_unique.py`
    - Update variables: `base_path`
3. Convert alphaN to N for all columns post making random map - `alphaN_to_N.py`
    - Update variables: `base_path`
4. Convert all CSV to proto JSON - `prepare_txt_convert_to_openspec.py`
    - Update variables: `base_path`
5. Add missing keys to proto JSONs - `add_keys_proto_json.py`
    - Command : `python3 add_keys_proto_json.py --base-path <path>`
6. Add epoch timestamp in millis to folders - `add_epoch_timestamp_open_spec.py`
    - Update variables: `base_path`
7. Prepare latest watermark JSONs - `prepare_watermark_json.py`
    - Update variables: `base_path` & `scenario_combination`
