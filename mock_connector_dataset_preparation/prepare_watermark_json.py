import json
import os
import shutil
import copy


def divide_json_into_files(data, batch_size):
    objects = list(data.keys())
    num_files = len(objects) // batch_size + 1

    for i in range(num_files):
        start_index = i * batch_size
        end_index = (i + 1) * batch_size
        batch_objects = objects[start_index:end_index]
        batch_data = {obj: data[obj] for obj in batch_objects}

        filename = f"{str((i+1)).zfill(3)}_latest_watermarks.json"
        with open(os.path.join(watermark_directory, filename), "w") as file:
            json.dump(batch_data, file, indent=2)

        print(f"Created {filename} with {len(batch_objects)} objects.")

    return num_files

watermark_object = {
  "book": "",
  "beginExclusiveWatermark": "",
  "endInclusiveWatermark": "",
  "bulkObjectLocation": {
    "resourceProvider": "STORAGE_PROVIDER_AWS_S3"
  },
  "expiration": "2024-06-05T14:04:55.501Z"
}

scenario_combination = {'XXS': 740, 'XS': 500}
base_path = '/tmp/pnl_pdf/business_data_directory'

watermark_directory = os.path.join(base_path, '..', 'watermarks_scenario_directory/all_watermarks')
shutil.rmtree(watermark_directory, ignore_errors=True)
os.makedirs(watermark_directory)

folder_size_combination = {}
all_folders = os.listdir(base_path)
for folder in all_folders:
    size = folder.split('_')[1]
    if size in folder_size_combination:
        folder_size_combination[size].append(folder)
    else:
        folder_size_combination[size] = [folder]

folder_size_combination_sorted = {k: v for k, v in sorted(folder_size_combination.items(), key=lambda item: item[1])}
final_json_364 = {}
final_json_365 = {}
end_inclusive_watermark_364 = {}

for book_size, no_of_books in scenario_combination.items():
    if no_of_books > len(folder_size_combination_sorted[book_size]):
        raise ValueError("Requested scenario cannot be formatted.")
    for index in range(no_of_books):
        folder_name = folder_size_combination_sorted[book_size][index]
        book_id = folder_name.split('_')[0]
        for folder in os.listdir(os.path.join(base_path, folder_name)):
            if folder.endswith(f'random_364_btc_1'):
                t_end_inclusive_watermark_364 = folder.split('_')[0]
                break
        else:
            raise ValueError("Folder with the required suffix not found.")

        for folder in os.listdir(os.path.join(base_path, folder_name)):
            if folder.endswith(f'random_365_btc_1'):
                t_end_inclusive_watermark_365 = folder.split('_')[0]
                break
        else:
            raise ValueError("Folder with the required suffix not found.")

        book_dict = copy.deepcopy(watermark_object)
        del book_dict['beginExclusiveWatermark']
        book_dict['book'] = book_id
        book_dict['endInclusiveWatermark'] = t_end_inclusive_watermark_364
        end_inclusive_watermark_364[book_id] = t_end_inclusive_watermark_364
        final_json_364[book_id] = book_dict

        book_dict = copy.deepcopy(watermark_object)
        book_dict['book'] = book_id
        book_dict['endInclusiveWatermark'] = t_end_inclusive_watermark_365
        final_json_365[book_id] = book_dict

n = divide_json_into_files(final_json_364, 100)

for key, tw_obj in final_json_365.items():
    final_json_365[key]['beginExclusiveWatermark'] = final_json_364[key]['endInclusiveWatermark']

with open(os.path.join(watermark_directory, f'{str(n+1).zfill(3)}_latest_watermarks.json'), 'w') as f:
    json.dump(final_json_365, f, indent=2)
