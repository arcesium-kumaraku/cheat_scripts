import pandas as pd
import time
import shutil
import os
import multiprocessing as mp


indexes = {'book_id': 10000, 'bundle_id': 10000, 'spn': 10000000, 'ca_id': 10000}
alphaN_to_N_map = {}
replace_dict = {}
field_map = {
        'security_bond.csv': ['spn'],
        'security_ccy_fwd.csv': ['spn'],
        'security_equity.csv': ['spn'],
        'security_futures.csv': ['spn'],
        'security_linker.csv': ['spn'],
        'security_option.csv': ['spn'],
        'security_swap.csv': ['spn'],
        'book.csv': ['book_id'],
        'bundle.csv': ['bundle_id'],
        'custodian_account.csv': ['ca_id']
    }
field_map_2 = {
        'taxlot_initializer.csv': ['spn', 'book_id', 'bundle_id', 'ca_id'],
        'fwd_rates.csv': ['spn'],
        'mat.csv': ['spn'],
        'prices.csv': ['spn'],
        'trades.csv': ['spn', 'book_id', 'bundle_id', 'ca_id'],
        'reorgs.csv': ['spn', 'book_id', 'bundle_id', 'ca_id'],
        'vrs.csv': ['spn'],
        'security_bond.csv': ['spn'],
        'security_ccy_fwd.csv': ['spn'],
        'security_equity.csv': ['spn'],
        'security_futures.csv': ['spn'],
        'security_linker.csv': ['spn'],
        'security_option.csv': ['spn', 'underlying', 'deliverable'],
        'security_swap.csv': ['spn', 'underlying'],
        'book.csv': ['book_id'],
        'bundle.csv': ['bundle_id'],
        'custodian_account.csv': ['ca_id'],
        'transfer.csv': ['book_id', 'source_book_id', 'destination_book_id', 'spn', 'destination_bundle_id', 'source_bundle_id', 'destination_ca_id', 'source_ca_id'],
        'resets.csv': ['spn', 'book_id', 'ca_id']
    }
special_cases = {
    'security_option.csv': {'underlying': 'spn', 'deliverable': 'spn'},
    'security_swap.csv': {'underlying': 'spn'},
    'transfer.csv': {
        'destination_bundle_id': 'bundle_id',
        'source_bundle_id': 'bundle_id',
        'destination_ca_id': 'ca_id',
        'source_ca_id': 'ca_id',
        'source_book_id': 'book_id',
        'destination_book_id': 'book_id'
    }
}


def create_map(folders_and_files):
    for folder, files in folders_and_files.items():
        folder_name = folder.split(os.path.sep)[-3]
        if folder_name not in alphaN_to_N_map:
            alphaN_to_N_map[folder_name] = {}
        for file_name in files:
            if file_name in field_map:
                file_path = os.path.join(folder, file_name)
                df = pd.read_csv(file_path, keep_default_na=False)
                for column in field_map[file_name]:
                    if column in df.columns:
                        dict_to_be_updated = {value: index + indexes[column] + 1 for index, value in enumerate(df[column].unique())}
                        if column not in alphaN_to_N_map[folder_name]:
                            alphaN_to_N_map[folder_name][column] = {}
                        alphaN_to_N_map[folder_name][column].update(dict_to_be_updated)
                        indexes.update({column: max(alphaN_to_N_map[folder_name][column].values())})


def prepare_replace_dict(folder, file):
    for file_name, columns in field_map_2.items():
        for column in columns:
            if file_name in special_cases and column in special_cases[file_name]:
                replace_dict[column] = alphaN_to_N_map[folder][special_cases[file_name][column]]
            else:
                replace_dict[column] = alphaN_to_N_map[folder][column]

    return {key: replace_dict[key] for key in field_map_2[file]}


def process_folder(folders_and_files):
    for folder, files in folders_and_files.items():
        print(f'Processing folder: {folder}')
        for file_name in files:
            file_path = os.path.join(folder, file_name)
            if file_name in field_map_2:
                t_dict = prepare_replace_dict(folder.split(os.path.sep)[-3], file_name)
                df = pd.read_csv(file_path, keep_default_na=False)
                df.replace(t_dict, inplace=True, regex=False)
                df.to_csv(file_path, index=False)


def process_folders_in_parallel(folders_and_files):
    with mp.Pool(processes=mp.cpu_count()) as pool:
        pool.map(process_folder, folders_and_files)
        pool.close()
        pool.join()
    

def rename_folder(base_path):
    for folder in os.listdir(base_path):
        print(f'Renaming folder: {folder}')
        size = folder.split('_')[0]
        for book_name in alphaN_to_N_map[folder]['book_id']:
            if folder == book_name.split('_', 2)[-1]:
                book_id = alphaN_to_N_map[folder]['book_id'][book_name]
                shutil.move(os.path.join(base_path, folder), os.path.join(base_path, f'{book_id}_{size}'))


if __name__ == '__main__':
    t0 = time.time()

    base_path = '/tmp/pnl_pdf/csv_final_dataset_1'

    folders_and_files = [{root: [file for file in files if file.endswith('.csv')]} for root, dirs, files in os.walk(base_path) if 'input' in root]
    for t_dict in folders_and_files:
        create_map(t_dict)
    print(f'Map: {alphaN_to_N_map}')
    print(f'Indexes: {indexes}')
    process_folders_in_parallel(folders_and_files)
    rename_folder(base_path)

    print('Time taken:', time.time() - t0)
