import shutil
import os


start_epoch_mills = 1577858482069

base_path = '/tmp/pnl_pdf/business_data_directory'
for book_folder in os.listdir(base_path):
    t_stamp = start_epoch_mills + 10000
    random_folder_list = [i for i in os.listdir(os.path.join(base_path, book_folder)) if os.path.isdir(os.path.join(base_path, book_folder, i))]
    
    shutil.move(os.path.join(base_path, book_folder, 'scenario-ref-data'), os.path.join(base_path, book_folder, f'{t_stamp}_scenario-ref-data'))
    random_folder_list.remove('scenario-ref-data')
    
    t_stamp = t_stamp + 10000
    shutil.move(os.path.join(base_path, book_folder, 'lot_initializer_day_1'), os.path.join(base_path, book_folder, f'{t_stamp}_lot_initializer_day_1'))
    random_folder_list.remove('lot_initializer_day_1')
    for day_folder in sorted(random_folder_list, key=lambda s: int(s.split('_')[1])):
        t_stamp = t_stamp + 10000
        shutil.move(os.path.join(base_path, book_folder, day_folder), os.path.join(base_path, book_folder, f'{t_stamp}_{day_folder}'))
    
    start_epoch_mills = start_epoch_mills + 10000
