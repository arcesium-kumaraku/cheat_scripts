from pathlib import Path


if __name__ == '__main__':
    base_path = Path('/tmp/pnl_pdf/csv_final_dataset')

    folders = [folder_path for folder_path in base_path.glob('*')]
    for folder in folders:
        print(f"convert-to-openspec --repo {folder} --output-path {str(folder).replace('csv_final_dataset', 'business_data_directory')}_open_spec")
