import json
import os
import argparse
import multiprocessing as mp
from pathlib import Path
import time
from p_tqdm import p_map

def process_file(file_path):
    if file_path.name in ['trades.json', 'taxlot_initializer.json', 'bundle.json', 'custodian_account.json']:
        with open(file_path, 'r') as f:
            data = json.load(f)

        for t_data in data:
            if file_path.name.__contains__('trades.json'):
                t_data['transaction']['transaction_linked_security_attributes'] = dict()
                t_data['transaction']['transaction_linked_security_attributes']['birth_date'] = {"year": 1900, "month": 1, "day": 1}
                t_data['transaction']['transaction_date'] = t_data['transaction']['settle_date']

            elif file_path.name.__contains__('taxlot_initializer.json'):
                t_data['transaction']['transaction_currency_id'] = ''
                t_data['transaction']['transaction_linked_security_attributes'].update({'underlying_security_id': "", 'birth_date': {"year": 1900, "month": 1, "day": 1}})
                t_data['transaction']['transaction_date'] = t_data['transaction']['settle_date']

            elif file_path.name.__contains__('bundle.json'):
                t_data['reference_data']['bundle']['creation_date'] = {"year": 1900, "month": 1, "day": 1}

            elif file_path.name.__contains__('custodian_account.json'):
                t_data['reference_data']['custodian_account']['creation_date'] = {"year": 1900, "month": 1, "day": 1}

        with open(file_path, 'w') as f:
            json.dump(data, f, indent=2)


def add_keys(base_path):
    file_paths = []
    for file_path in Path(base_path).rglob('*.json'):
        file_paths.append(file_path)
    
    p_map(process_file,file_paths)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--base-path', dest='base_path', required=True)
    args = parser.parse_args()

    t0 = time.time()

    if os.path.exists(args.base_path):
        add_keys(args.base_path)
    
    print('Time taken:', time.time() - t0)


if __name__ == '__main__':
    '''
    python3 add_keys_proto_json.py --base-path /home/guptaksh/Documents/JSON_TEST_SHORT-24_final_v6
    '''

    main()
