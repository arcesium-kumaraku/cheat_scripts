import pandas as pd
import shutil
import time
import resource
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path


def copy_files(src_path, dest_path):
    shutil.copytree(src_path, dest_path)


def main():
    t0 = time.time()
    INDEX = 1

    soft_limit, hard_limit = resource.getrlimit(resource.RLIMIT_NOFILE)
    resource.setrlimit(resource.RLIMIT_NOFILE, (hard_limit, hard_limit))

    base_path = Path('/tmp/pnl_pdf')
    dest_dataset_path = base_path / 'csv_final_dataset'

    shutil.rmtree(dest_dataset_path, ignore_errors=True)
    dest_dataset_path.mkdir(parents=True, exist_ok=True)

    dataset_combination = {'XXS': [base_path / 'csv_raw_data/TEST_XXS', 740],
                        'XS': [base_path / 'csv_raw_data/TEST_XS', 500]}

    with ThreadPoolExecutor() as executor:
        tasks = [
            executor.submit(copy_files, value[0], dest_dataset_path / f'{key}_{index}')
            for key, value in dataset_combination.items()
            for index in range(INDEX, value[-1] + INDEX)
            if value[0].exists()
        ]
        for task in tasks:
            task.result()

    print('Time taken: ', time.time() - t0)


if __name__ == '__main__':
    main()
