import pandas as pd
import os
import math
from tqdm import tqdm
import time
import argparse


def csf_multiload_distribution(base_path):
    for folder in tqdm([i for i in os.listdir(base_path) if os.path.isdir(os.path.join(base_path, i))]):
        # print(folder)
        if folder.endswith('3'):
            for file in os.listdir(os.path.join(base_path, folder, 'input')):
                if file in ['prices.csv', 'ccy_fwd.csv', 'fx_rates.csv', 'fwd_rates.csv', 'resets.csv', 'transfer.csv']:
                    file_path = os.path.join(base_path, folder, 'input', file)
                    df = pd.read_csv(file_path, keep_default_na=False)
                    os.remove(file_path)
                    
                    df_new = df[df['operation_code'].isin(['NEW'])]
                    df_new.to_csv(os.path.join(base_path, f'{folder[:-1]}1', 'input', file), index=False)

                    df_amend = df[df['operation_code'].isin(['AMEND'])]
                    df_amend_1 = df_amend.head(math.ceil(len(df_amend.index) * 0.5))
                    df_amend_2 = df_amend.tail(int(len(df_amend.index) * 0.5))

                    df_cancel = df[df['operation_code'].isin(['CANCEL'])]
                    df_cancel_1 = df_cancel.head(math.ceil(len(df_cancel.index) * 0.5))
                    df_cancel_2 = df_cancel.tail(int(len(df_cancel.index) * 0.5))

                    df_2 = pd.concat([df_amend_1, df_cancel_1], ignore_index=True)
                    if len(df_2.index) > 0:
                        df_2.to_csv(os.path.join(base_path, f'{folder[:-1]}2', 'input', file), index=False)

                    df_3 = pd.concat([df_amend_2, df_cancel_2], ignore_index=True)
                    if len(df_3.index) > 0:
                        df_3.to_csv(os.path.join(base_path, f'{folder[:-1]}3', 'input', file), index=False)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--base-path', dest='base_path', required=True)
    args = parser.parse_args()

    if os.path.exists(args.base_path):
        csf_multiload_distribution(args.base_path)


if __name__ == '__main__':
    '''
    python3 csf_multiload_distribution.py --base-path /home/guptaksh/Documents/TS_amends/S_batch_v1
    '''
    t0 = time.time()
    main()
    print(f'{time.time() - t0} seconds')
