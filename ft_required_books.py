import pandas as pd
from yaml.loader import SafeLoader
import yaml
import os
import json
import shutil


ft_yaml_path = '/home/guptaksh/code/trinity-engine/test-driver/src/test/resources/functional-tests-collection.yaml'
test_data_provider = '/home/guptaksh/code/test-data-provider/test_data/test_scenarios'
test_case = '148_SL_reorg_equity'


with open(ft_yaml_path) as f:
    ft_yaml = yaml.load(f, Loader=SafeLoader)

test_scenarios = ft_yaml['testScenarios'][test_case]

scenario_ref_data_path = os.path.join(test_data_provider, test_case, 'scenario-ref-data')

for test_scenario in test_scenarios:
    test_scenario_path = os.path.join(test_data_provider, test_case, test_scenario)

    # manifest
    with open(os.path.join(test_scenario_path, 'manifest.json')) as manifest_obj:
        manifest_dict = json.load(manifest_obj)

    if 'parent' in manifest_dict:
        del manifest_dict['parent']

        with open(os.path.join(test_scenario_path, "manifest.json"), "w") as m_f:
            json.dump(manifest_dict, m_f, indent=2)

    if os.path.exists(scenario_ref_data_path):
        for file in [files for files in os.listdir(os.path.join(scenario_ref_data_path, 'input')) if files.endswith('.csv')]:
            shutil.copy(os.path.join(scenario_ref_data_path, 'input', file), os.path.join(test_scenario_path, 'input', file))

    book_list = list()
    book_path = os.path.join(test_scenario_path, 'input', 'book.csv')
    book_df = pd.read_csv(book_path, keep_default_na=False)
    txn_file_names = ['trades.csv', 'resets.csv', 'transfer.csv', 'reorgs.csv', 'mat.csv', 'ccy_fwd.csv',
                      'taxlot_initializer.csv', 'resets.csv', 'cost_qty_adjust.csv', 'spots.csv']
    for txn_file in txn_file_names:
        txn_file_path = os.path.join(test_scenario_path, 'input', txn_file)
        if os.path.exists(txn_file_path):
            txn_df = pd.read_csv(txn_file_path, keep_default_na=False)
            book_list.extend(txn_df['book_id'].unique())
    book_df = book_df[book_df['book_id'].isin(list(set(book_list)))]

    book_df.to_csv(book_path, index=False)
